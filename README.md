# pg-stat
Simple scripts to keep snapshots of PostgreSQL statistics

## Take snapshot
Execute:
```psql -U tad -h localhost -p 5432 -d mydb -f stats_snapshot.sql```

## Create customer
```insert into customer (customer_id, code, name, pg_version) values (1, 'XX', 'xxx', '10');```

## Import snapshot
Execute:
```select import_snapshot ('XX', '/tmp/pg_stat_user_tables_2019-03-18_12:00:55.copy');```
